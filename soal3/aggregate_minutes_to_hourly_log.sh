#!/bin/bash

#3 c
# mkdir -p hasilc
# free -m > test.log
# du -sh /home/labib/teps >>  test.log
# awk 'BEGIN {ORS=",";OFS=","} NR==2 {print $2 ,$3,$4,$5,$6,$7} NR==3 {print $2,$3,$4} NR==4 {print $2, $1}' test.log >> tap.log
# rm test.log
# mas=$(awk '{gsub(/,$/,"");print}' tap.log)
# rm tap.log
# echo "$mas"

max=$(sort /home/labib/tes.log | tail -1)
min=$(sort /home/labib/tes.log | head -1)
avg=$(awk 'BEGIN{FS=","} 
{t1+=$1;t2+=$2;t3+=$3;t4+=$4;t5+=$5;t6+=$6;t7+=$7;t8+=$8;t9+=$9;t11+=$11} 
END{printf "%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%s,%s",t1/(NR),t2/(NR),t3/(NR),t4/(NR),t5/(NR),t6/(NR),t7/(NR),t8/(NR),t9/(NR),$10,t11/(NR)}' /home/labib/tes.log )
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
echo "maximum,$max"
echo "minimum,$min"
echo "average,$avg"
#crontab
# */1 * * * * bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/att.sh >> /home/labib/tes.log
# */3 * * * * bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/aggregate_minutes_to_hourly_log.sh >> /home/labib/metrics_`date +\%Y\%m\%d\%H`.log

#3 d
# */1 * * * * (bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/minute_log.sh >> /home/labib/metrics_`date +\%Y\%m\%d\%H\%M\%S`.log) && (chmod 400 /home/labib/metrics_`date +\%Y\%m\%d\%H\%M\%S`.log)
# */1 * * * * (bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/att.sh >> /home/labib/tes.log) && (chmod 400 /home/labib/tes.log)
# */3 * * * * (bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/aggregate_minutes_to_hourly_log.sh >> /home/labib/hasil1.log) && (chmod 400 /home/labib/metrics_`date +\%Y\%m\%d\%H`.log

