#!/bin/bash

#soal 1.a
echo -e "username : \c"
read -r username

#soal 1.b
flag=0
while [ $flag -ne 1 ]
do
    mkdir -p users  
    touch users/user.txt
    if grep -q "_$username" ./users/user.txt; then
    echo "Username sudah ada"
    echo "`date` REGISTER: ERROR User already exists" >> log.txt
    flag=`expr $flag + 1`
    else
        echo -e "password : "
        read -s -r password
        if [[ ${#password} -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* && "$password" != *"$username"* ]];then
        flag=`expr $flag + 1` 
        echo "_$username-$password" >> ./users/user.txt
        echo "Registrasi Berhasil"
        echo "`date` REGISTER: INFO User $username registered successfully" >> log.txt
        else
        echo "Password Minimal terdiri dari 8 karakter, 1 huruf kapital dan 1 huruf kecil, bersifat alphanumerical, dan tidak mengandung username"
        echo -e "password : \c"
        read -s -r password
        fi 
    fi
done



