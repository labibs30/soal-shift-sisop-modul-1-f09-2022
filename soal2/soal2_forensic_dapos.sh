#!/bin/bash

DIRECTORY_FILE_RESULT="forensic_log_website_daffainfo_log"
AVG_FILE="ratarata.txt"
RESULT_FILE="result.txt"

if [ ! -d "${DIRECTORY_FILE_RESULT}" ]
then
	mkdir "${DIRECTORY_FILE_RESULT}"
fi

# No. 1

awk -F: '{if ($1 != "\"IP\""){sum+=1;}}END{print "Rata-rata serangan adalah sebanyak " sum/($3+1), "requests per jam";}' ../log_website_daffainfo.log > ${DIRECTORY_FILE_RESULT}/${AVG_FILE}

# cat ../log_website_daffainfo.log | cut -d[ -f2 | cut -d] -f1 | awk -F: '{if (($3 != "\"Request\"")) {print $3":00"}}' | sort -n | uniq -c

# No. 2 dan 3

awk '
	BEGIN {
		FS = ":"
		mostReq = "NULL"
		amountOfReq = 0

		curlReq = 0
	}
	{
		if ($1 != "\"IP\"")
		{
			temp[$1]++
			if (amountOfReq < temp[$1])
			{
				mostReq = $1
				amountOfReq = temp[$1]
			}

			if (substr($9, 2, 4) == "curl")
			{
				curlReq++
			}

			if ($3 == "02")
			{
				ipAddressAccessIn2AM[$1] = TRUE
			}
		}
	}
	END {
		print "IP yang paling banyak mengakses server adalah:", mostReq, "sebanyak", temp[mostReq], "requests\n"
		print "Ada", curlReq, "request yang menggunakan curl sebagai user agent\n"

		for (i in ipAddressAccessIn2AM)
		{
			print i
		}
	}
' ../log_website_daffainfo.log > "${DIRECTORY_FILE_RESULT}/${RESULT_FILE}"
