# soal-shift-sisop-modul-1-F09-2022

Anggota Kelompok :
* Gaudhiwaa Hendrasto	5025201066
* M Labib Alfaraby      5025201083
* Ilma Fahma Syadidah	5025201063

***
1. Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

    - Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh
    - Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut:
        * Minimal 8 karakter
        * Memiliki minimal 1 huruf kapital dan 1 huruf kecil
        * Alphanumeric
        * Tidak boleh sama dengan username
    - Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss **MESSAGE**. Message pada log akan berbeda tergantung aksi yang dilakukan user.
        1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
        2. Ketika percobaan register berhasil, maka message pada log adalah **REGISTER**: INFO User **USERNAME** registered successfully
        3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user **USERNAME**
        4. Ketika user berhasil login, maka message pada log adalah **LOGIN**: INFO User **USERNAME** logged in
    - Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
        * **dl N** ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama **YYYY-MM-DD_USERNAME**. Gambar-gambar yang didownload juga memiliki format nama **PIC_XX**, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
        * **att**
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.
---
# Penyelesain soal 1
- `Nomor 1 a` sistem register dan login yang dibuat terdiri dari, username dan password. Pada bagian a, kita langsung menyimpan hasil register yang berhasil ke dalam file ./users/user.txt, dengan bentuk penyimpanan (_username-password) menggunakan echo. Berikut adalah script register pada file register.sh bagian a :
    ```shell
    #!/bin/bash
    echo -e "username : \c"
    read -r username
    echo -e "password : \c"
    read -s -r password
    echo "$_username-$password" >> ./users/user.txt
    ```
    Kemudian, script login pada file main.sh bagian a :
    ```shell
    #!/bin/bash
    echo -e "username : \c"
    read -r username
    echo -e "password : \c"
    read -s -r password
    echo ""
    ```
    Berikut adalah hasil 1a :
    ![hasil](./screenshot/1_a.png)
    Konsepnya menyimpan hasil inputan dari user menggunakan operator redirection (>>) agar inputan dengan username lain bisa disimpan pada baris selanjutnya.
- `Nomor 1 b` Bagian b merupakan pengembangan dari bagian a, dimana kita akan memberikan kriteria untuk password yang akan di register dan login. Untuk sistem register bagian b pada file register.sh, sebagai berikut : 
    ```shell
    #!/bin/bash

    echo -e "username : \c"
    read -r username
    flag=0
    while [ $flag -ne 1 ]
    do
        mkdir -p users  
        touch users/user.txt
        if grep -q "_$username" ./users/user.txt; then
        echo "Username sudah ada"
        flag=`expr $flag + 1`
        else
            echo -e "password : "
            read -s -r password
            if [[ ${#password} -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* && "$password" != *"$username"* ]];then
            flag=`expr $flag + 1` 
            echo "_$username-$password" >> ./users/user.txt
            echo "Registrasi Berhasil"
            else
            echo "Password Minimal terdiri dari 8 karakter, 1 huruf kapital dan 1 huruf kecil, bersifat alphanumerical, dan tidak mengandung username"
            echo -e "password : \c"
            read -s -r password
            fi 
        fi
    done
    ```
    Pada bagian b ini, user hanya bisa melakukan register apabila username belum ada pada ./users/user.txt. Lalu, apabila username belum tersedia, maka user bisa meinginputkan password dengan kriteria di atas hingga register berhasil (terjadi looping hingga kondisi kriteria terpenuhi). Untuk memberi persyaratan kriteria menggunakan regular expression berikut:
    
    `${#password} -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* && "$password" != *"$username"*`
    
    Kemudian, untuk script login pada file main.sh bagian b :
    ```shell
    #!/bin/bash
    echo -e "username : \c"
    read -r username
    echo -e "password : \c"
    read -s -r password
    echo ""
    if grep -q "_$username-$password" ./users/user.txt; then
        #komen
    else
        echo "Password Anda Salah"
        #count_login=$(($count_login + 1))
    fi
    ```
    Pengecekan login berhasil / tidak menggunakan `grep`, yang berfungsi untuk menampilkan setiap baris pada suatu file yang mengandung kata yang dicari,  untuk pasangan usernama (spasi) password pada file ./users/user.txt

- `Nomor 1 c` bagian c juga merupakan pengembangan dari sistem yang dibuat, yaitu dengan adanya catalog proses registrasi dan login. Yang mana akan menyimpan semua hasil dari proses registrasi dan login apakah berhasil atau tidak, sesuai dengan penjelasan soal.
    Berikut, script registrasi.sh bagian c:
    ```shell
    #!/bin/bash

    #soal 1.a
    echo -e "username : \c"
    read -r username

    #soal 1.b
    flag=0
    while [ $flag -ne 1 ]
    do
        mkdir -p users  
        touch users/user.txt
        if grep -q "_$username" ./users/user.txt; then
        echo "Username sudah ada"
        echo "`date` REGISTER: ERROR User already exists" >> log.txt
        flag=`expr $flag + 1`
        else
            echo -e "password : "
            read -s -r password
            if [[ ${#password} -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* && "$password" != *"$username"* ]];then
            flag=`expr $flag + 1` 
            echo "_$username-$password" >> ./users/user.txt
            echo "Registrasi Berhasil"
            echo "`date` REGISTER: INFO User $username registered successfully" >> log.txt
            else
            echo "Password Minimal terdiri dari 8 karakter, 1 huruf kapital dan 1 huruf kecil, bersifat alphanumerical, dan tidak mengandung username"
            echo -e "password : \c"
            read -s -r password
            fi 
        fi
    done
    ```
    Kemudian, script main.sh bagian c:
    ```shell
        #!/bin/bash
    echo -e "username : \c"
    read -r username
    echo -e "password : \c"
    read -s -r password
    echo ""
    if grep -q "_$username-$password" ./users/user.txt; then
        echo "`date` LOGIN: INFO User $username logged in" >> log.txt
    else
        echo "Password Anda Salah"
        #count_login=$(($count_login + 1))
        echo "`date` LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    fi
    ```
    Berikut adalah hasil 1c :
    ![hasil](./screenshot/1_c.png)
    Untuk menyimpannya pada log.txt digunakan command `echo` lalu redirection ke log.txt
- `Nomor 1 d` bagian d juga merupakan pengembangan untuk sistem login, yang mana terdapat dua opsi, yaitu dl untuk mendownload gambar dengan proses dan ketentuan yang berlaku serta att menampilkan total percobaan login seorang user. Berikut script login.sh bagian d :
    ```shell
    #!/bin/bash
    echo -e "username : \c"
    read -r username
    echo -e "password : \c"
    read -s -r password
    echo ""
    if grep -q "_$username-$password" ./users/user.txt; then
        echo "`date` LOGIN: INFO User $username logged in" >> log.txt
        echo -e "Masukkin perintah : \c"
        read -r order
        n=$(find "`(date +%F)`_$username" -type f | wc -l)
        if [[ "$order" == "dl" ]];then
            echo -e "Masukkan jumlah download file : \c"
            read -r N 
            mkdir -p "`(date +%F)`_$username" 
            unzip -r "`(date +%F)`_$.zip"
            n=$(($n + 1))
            for ((num=$n; num<`expr $N + $n`; num=num+1))
            do
                if [ $num -lt 10 ];then
                    curl https://loremflickr.com/cache/resized/65535_51539523192_aab959e831_n_320_240_nofilter.jpg >> ./`(date +%F)`_$username/"PIC_0$num".jpg
                else
                    curl https://loremflickr.com/cache/resized/65535_51539523192_aab959e831_n_320_240_nofilter.jpg >> ./`(date +%F)`_$username/"PIC_$num".jpg
                fi
            done
            zip -P $password -r "`(date +%F)`_$username.zip" "`(date +%F)`_$username"
            #echo "$n"
            #rm -r  "`(date +%F)`_$username"
            #echo $n
        else
            grep "LOGIN" log.txt | grep -c "$username"
        #grep -c "LOGIN" log.txt
        fi
    else
        echo "Password Anda Salah"
        #count_login=$(($count_login + 1))
        echo "`date` LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    fi
    ```
    Setelah user memasukkan jumlah download, maka sebuah folder akan di buat untuk menampung file hasil download. Karena penamaan gambar yang cukup unik, maka dilakukan `if else` apabila nomor gambar kurang dari 10 dan sebaliknya.
    Berikut adalah hasil 1d :
    ![hasil](./screenshot/1_d.png)
    ![hasil](./screenshot/1_d2.png)
    
2. Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama **"soal2_forensic_dapos.sh"** untuk melaksanakan tugas-tugas berikut:

    - Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.
    - Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.
    - Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.
    - Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.
    - Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

# Penyelesaian Nomor 2
- Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log. Dengan cara membuat sebuah file script awk bernama "soal2_forensic_dapos.sh".
```bash
nano soal2_forensic_dapos.sh
```
Berisikan :
```bash
#!/bin/bash
DIRECTORY_FILE_RESULT="forensic_log_website_daffainfo_log"
AVG_FILE="ratarata.txt"
RESULT_FILE="result.txt"

if [ ! -d "${DIRECTORY_FILE_RESULT}" ]
then
	mkdir "${DIRECTORY_FILE_RESULT}"
fi
```
Kita inisialisiasi terlebih dahulu supaya lebih mudah dalam menyimpan file nantinya dengan DIRECTORY_FILE_RESULT, AVG_FILE, dan RESULT_FILE. Kemudian masukkan nama folder forensic_log_website_daffainfo_log, nama file ratarata.txt dan result.txt. 

Jika belum terdapat folder forensic_log_website_daffainfo_log, maka script tersebut menjalankan pembuatan folder. 

- Untuk mengetahui serangan yang diluncurkan ke website https://daffa.info kita membuat script awk sebagai berikut :
```bash
awk -F: '{if ($1 != "\"IP\""){sum+=1;}}END{print "Rata-rata serangan adalah sebanyak " sum/($3+1), "requests per jam";}' ../log_website_daffainfo.log > ${DIRECTORY_FILE_RESULT}/${AVG_FILE}
```
awk -F memberi tahu awk pemisah bidang apa yang digunakan. Dalam kasus ini, -F: berarti pemisahnya adalah : (titik dua). Bisa dilihat di file log_website_daffainfo.log, pemisahnya adalah : (titik dua). Kemudian {if ($1 != "\"IP\""){sum+=1;}} berguna untuk menjumlahkan serangan seluruhnya yaitu 973 requests, dan dimasukkan ke dalam variabel sum. Setelah itu sum/($3+1) akan menampilkan rata-rata serangan per jam, di mana $3 adalah berapa banyak jam yang tertera dalam file tersebut (tidak termasuk nol, sehingga perlu kita tambahkan 1), yakni 13 (jam 0-12). Maka rata-rata yang dihitung adalah 973/13 = ~74.8462 requests per jam.

Untuk memastikan hal itu benar kita bisa mengeksekusi script :

```bash
cat ../log_website_daffainfo.log | cut -d[ -f2 | cut -d] -f1 | awk -F: '{if (($3 != "\"Request\"")) {print $3":00"}}' | sort -n | uniq -c
```

Kemudian akan menampilkan banyaknya IP setiap jam, sebagai berikut :

```bash
  10 00:00
 546 01:00
  88 02:00
   8 03:00
   4 04:00
   2 05:00
   8 06:00
   9 07:00
   9 08:00
  15 09:00
   7 10:00
 266 11:00
   1 12:00
```

Jika kita jumlahkan serangannya maka totalnya adalah 973 dan dalam catatan tersebut terdapat 13 jam. Sehingga rata-ratanya adalah 973/13.

Sehingga hasil dari file ratarata.txt adalah :
![hasil 2b](./screenshot/2_b.png)

- IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut, bisa dilakukan dengan cara:
```bash
awk '
	BEGIN {
		FS = ":"
		mostReq = "NULL"
		amountOfReq = 0

		curlReq = 0
	}
	{
		if ($1 != "\"IP\"")
		{
			temp[$1]++
			if (amountOfReq < temp[$1])
			{
				mostReq = $1
				amountOfReq = temp[$1]
			}

			if (substr($9, 2, 4) == "curl")
			{
				curlReq++
			}

			if ($3 == "02")
			{
				ipAddressAccessIn2AM[$1] = 1
			}
		}
	}
	END {
		print "IP yang paling banyak mengakses server adalah:", mostReq, "sebanyak", temp[mostReq], "requests\n"
		print "Ada", curlReq, "request yang menggunakan curl sebagai user agent\n"

		for (i in ipAddressAccessIn2AM)
		{
			print i
		}
	}
' ../log_website_daffainfo.log > "${DIRECTORY_FILE_RESULT}/${RESULT_FILE}"
```

Inisialisasi FS = ":" untuk separator, mostReq = "NULL" untuk request IP terbanyak, amountOfReq = 0 untuk mengetahui berapa banyak IP terbanyak melakukan request, dan curlReq = 0 untuk mengetahui berapa banyak request yang menggunakan user-agent curl.

Jika $1 (IP Address) tidak sama dengan \IP\ maka kita buat variabel temp[$1] (variabel temp yang beralamat IP) ditambahkan dengan 1. Sehingga setiap alamat IP dapat kita hitung ada berapa banyak request yang ia lakukan. Jika terdapat temp[$1] yang jumlahnya lebih besar dari temp[$1] sebelumnya, maka mostReq (request terbanyak) akan berganti pada temp[$1] tersebut. Kemudian tampilkan "IP yang paling banyak mengakses server adalah:", mostReq, "sebanyak", temp[mostReq], "requests\n".

```bash 
    if ($1 != "\"IP\"")
		{
			temp[$1]++
			if (amountOfReq < temp[$1])
			{
				mostReq = $1
				amountOfReq = temp[$1]
			}
        }
        END {
		print "IP yang paling banyak mengakses server adalah:", mostReq, "sebanyak", temp[mostReq], "requests\n"
	}
```

- Untuk mengetahui berapa banyak requests yang menggunakan user-agent curl, maka kita bisa gunakan script :

```bash
if ($1 != "\"IP\"")
    {
        if (substr($9, 2, 4) == "curl")
			{
				curlReq++
			}
    }
    END {
		print "Ada", curlReq, "request yang menggunakan curl sebagai user agent\n"
	}
```

Variabel $9 akan memunculkan "curl/7.54.0". Untuk mendapat curl saja, kita gunakan substr($9, 2, 4) yaitu substr(variabel, awal huruf dimulai, berapa banyak huruf) sehingga menghasilkan curl. Jika ditemukan maka tambahkan ke dalam curlReq dan kita bisa melihat berapa banyak requests yang menggunakan user-agent curl. Kemudian tampilkan "Ada", curlReq, "request yang menggunakan curl sebagai user agent\n".

- Untuk mengetahui IP yang mengakses website pada jam 2 pagi tanggal 23, kita bisa mengeksekusi script berikut :

```bash
if ($1 != "\"IP\"")
    {
        if ($3 == "02")
			{
				ipAddressAccessIn2AM[$1] = TRUE
			}
    }
    END {
		for (i in ipAddressAccessIn2AM)
		{
			print i
		}
	}
```

Variabel $3 adalah jam pada saat IP mengakses website. Jika ditemukan IP yang mengkases pada jam "02", maka akan ditampilkan setiap IP-nya. 

Hasil dari file result.txt adalah :
![hasil 2cde](./screenshot/2_cde.png)

---
3. Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

    Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.
    - Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
    - Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
    - Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
    - Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.
# Penyelesaian soal 3
- `Nomor 3 a` Pada bagian a, hasil monitoring ram dan directory dapat langsung di simpan di sebuah file `metrics_{YmdHms}.log` dengan menggunakan operator redirection (> & >>), berikut script bagian a :
    ```shell
    #!/bin/bash
    free -m > test.log
    du -sh /home/labib/teps >>  test.log
    awk 'BEGIN {ORS=",";OFS=","} NR==2 {print $2 ,$3,$4,$5,$6,$7} NR==3 {print $2,$3,$4} NR==4 {print $2, $1}' test.log >> tap.log
    rm test.log
    mas=$(awk '{gsub(/,$/,"");print}' tap.log)
    rm tap.log
    echo "$mas"
    ```
- `Nomor 3 b` Pada bagian b, merupakan pengembangan dari bagian a yang mana monitoring dilakukan tiap menitnya, dengan mengikuti peraturan pendataan seperti pada soal, berikut scriptnya :
    ```shell
    #crontab
    */1 * * * * bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/minute_log.sh >> /home/labib/metrics_`date +\%Y\%m\%d\%H\%M\%S`.log
    ```
    scrip menggunakan `awk` untuk membentuk model pendataan hasil monitoring seperti pada ketentuan soal, untuk menjalankan menggunakan crontab sehingga program dapat berjalan setiap menitnya dengan melakukan pengaturan pada crontab. Script Crontab seperti pada baris yang di komen (hanya contoh menyesuaikan directory masing - masing)
    Berikut adalah hasil 3b :
    ![hasil](./screenshot/3_b.png)
- `Nomor 3 c` Pada bagian c diminta menampilkan aggregasi yang terdiri dari max, min, avg dari hasil monitoring ram dan directory untuk setiap jamnya. Berikut script untuk bagian c :
    ```shell
    #!/bin/bash

    #3 c
    # Untuk mengenerate hasil monitoring, dijalankan bersamaan dengan file aggregate dengan crontab dan dilakukan secara terpisah.
    mkdir -p hasilc
    free -m > test.log
    du -sh /home/labib/teps >>  test.log
    awk 'BEGIN {ORS=",";OFS=","} NR==2 {print $2 ,$3,$4,$5,$6,$7} NR==3 {print $2,$3,$4} NR==4 {print $2, $1}' test.log >> tap.log
    rm test.log
    mas=$(awk '{gsub(/,$/,"");print}' tap.log)
    rm tap.log
    echo "$mas"

    max=$(sort /home/labib/tes.log | tail -1)
    min=$(sort /home/labib/tes.log | head -1)
    avg=$(awk 'BEGIN{FS=","} 
    {t1+=$1;t2+=$2;t3+=$3;t4+=$4;t5+=$5;t6+=$6;t7+=$7;t8+=$8;t9+=$9;t11+=$11} 
    END{printf "%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%s,%s",t1/(NR),t2/(NR),t3/(NR),t4/(NR),t5/(NR),t6/(NR),t7/(NR),t8/(NR),t9/(NR),$10,t11/(NR)}' /home/labib/tes.log )
    echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
    echo "maximum,$max"
    echo "minimum,$min"
    echo "average,$avg"
    #crontab
    # */1 * * * * bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/att.sh >> /home/labib/tes.log
    # */3 * * * * bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/aggregate_minutes_to_hourly_log.sh >> /home/labib/metrics_`date +\%Y\%m\%d\%H`.log

    ```
    Untuk menentukan min dan max, digunakan fungsi short untuk menyortir hasil monitoring, lalu pipe dengan tail -1 untuk max dan pipe dengan head -1 untuk min, konsepnya sama seperti melakukan select * max(*) pada sql. Kemudian, untuk avg menggunakan `awk` dengan menjumlahkan setiap kolomnya dan mencari rerata tiap kolom lalu tampilkan hasil script.
    
    Berikut adalah hasil 3c :
    ![hasil](./screenshot/3_c.png)
    **Kekurangan** pada bagian c, yaitu diperlukan sebuah file lagi untuk mengenerate hasil monitoring ke dalam satu file, lalu baru dilakukan crontab monitoring agregasi setiap jamnya pada file tersebut.

- `Nomor 3 d` Pada bagian d, pemberian hak akses hanya pada user bersangkut dapat menggunakan `chmod 400 <path file>`, prosesnya dilakukan bersamaan ketika melakukan crontab setiap file log dengan menggunakan operator `&&` seperti berikut.
    ```shell
    #Bagian b
    # */1 * * * * (bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/minute_log.sh >> /home/labib/metrics_`date +\%Y\%m\%d\%H\%M\%S`.log) && (chmod 400 /home/labib/metrics_`date +\%Y\%m\%d\%H\%M\%S`.log)
    #Bagian c
    # */1 * * * * (bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/att.sh >> /home/labib/tes.log) && (chmod 400 /home/labib/tes.log)
    # 0 * * * * (bash /home/labib/soal-shift-sisop-modul-1-f09-2022/soal3/aggregate_minutes_to_hourly_log.sh >> /home/labib/hasil1.log) && (chmod 400 /home/labib/metrics_`date +\%Y\%m\%d\%H`.log


    ```
# Kendala yang dihadapi saat pengerjaan soal berlangsung
- Pada nomor 1d, pen-download gambar pada soal 1d melalui link setelah gambar diakses, tidak secara langsung melalui link dari soal.
- Pada nomor 3c, diperlukan sebuah file lagi untuk mengenerate hasil monitoring ke dalam satu file, lalu baru dilakukan crontab monitoring agregasi setiap jamnya pada file tersebut.
